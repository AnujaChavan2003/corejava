package Collection_Framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Copy_File {
	public static void main(String[] args) throws IOException  {
		Scanner sc = new Scanner(System.in);
		System.out.println("Provide file path which want to copy :");
		String filePath = sc.nextLine();
		File file = new File(filePath);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String newFilePath ="C:\\Users\\Admin\\eclipse-workspace\\CoreJava\\src\\Collection_Framework\\Pledge1";
		File fileToCopy = new File(newFilePath);
		FileWriter fw = new FileWriter(fileToCopy);
		BufferedWriter bw = new BufferedWriter(fw);
		String line;while((line =br.readLine())!= null) {
			System.out.println(line);
			bw.write(line);
			bw.write("\n");
			
		}
		bw.close();
		fw.close();
		br.close();
		fr.close();
		
	}

}
