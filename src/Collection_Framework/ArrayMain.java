package Collection_Framework;

public class ArrayMain {
	public static void main(String[] args) {
		int a=10;
		TextFile std = new TextFile();
		int p[]= new int[4];
		p[0]= 0;
	    p[1]= 2;
	    p[2]= 3;
	    p[3]= 5;
	   
	    System.out.println((p[3]+p[2]));
	    System.out.println("*** While loop***");
	    int i = 0;
	    while(i<4) {
	    	System.out.println(p[i]);
	    	i=i+1;
	    }
	    
	    	System.out.println("*** For loop***");
	    			for(i=0;i<4;i=i+1){
	    			System.out.println(p[i]);
	    }
	}
}


