package Example;

public class BinomialExpansion {
   public static void main(String[] args) {
	   equation2(4,3);  //function call
	   equation1(20,10);
	   equation3();
	   equation4();
   }
   
        public static void equation1(int p,int q)//parameterized
        {
        	int r = p+q;
        System.out.println(r);
        }
	   public static void equation2(int p,int q)//user defined function
	   {
   
	   int c;
	   c = p*p+2*p*q+q*q;
	   
	   System.out.println (c);
   }
	   public static void equation3()
	   {
		   int p =4;
		   int q = 5;
		   int r = p*p*p+3*p*p*q+3*p*q*q+q*q*q;
		   System.out.println(r);
		   
	   }
	   public static void equation4()
	   {
		   int p =2;
		   int q= 3;
		   int r = p*p*p*p+4*p*p*p*q+6*p*q*q*q+q*q*q*q;
		   
		   System.out.println(r);
	   }
}
