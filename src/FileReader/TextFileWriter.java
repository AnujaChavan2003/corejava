package FileReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;

public class TextFileWriter {
	
	public static void main(String[] args) throws IOException {
		
		String filepath = "C:\\Users\\Admin\\eclipse-workspace\\CoreJava\\src\\FileReader\\IndianArmy ";
		File textFile = new File(filepath);
		FileWriter fw = new FileWriter(textFile);//file overwrite & write from beginning
		FileWriter fw1 = new FileWriter(textFile,true);//append the data to existing file,write at end of file
		BufferedWriter bw = new BufferedWriter(fw1);
		bw.write("The Indian Army is the land-based branch and the largest component of the Indian Armed Forces.\n");
		bw.write("The President of India is the Supreme Commander of the Indian Army.");
		bw.close();
		
	}

}
