package FileReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TextFileUsingWhileLoop {
 

public static void main(String[] args) throws IOException {
	  String fileNameWithPath= "C:\\Users\\Admin\\eclipse-workspace\\CoreJava\\src\\FileReader\\Pledge";
	  File textfile = new File(fileNameWithPath);
	  FileReader fr= new FileReader(textfile);
	  BufferedReader br = new BufferedReader(fr);
	  
	  String line ="";
	  
	while(line!=null) {
		line = br.readLine();
		System.out.println(line);
	}
  }
}
