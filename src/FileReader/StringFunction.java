package FileReader;

public class StringFunction {
	public static void main(String[] args) {
		concatfunction();
		concatfunction1();
		replacefunction();
	}
	
	public static void concatfunction() {
		String name = "Anuja";
		name = name.concat(" Chavan");
		System.out.println(name);
	}
	public static void concatfunction1() {
		String line = "I Like ";
		line = line.concat(" Coding");
		System.out.println(line);
	}
    public static void replacefunction() {
    	String line ="I like Cricket";
    	line =line.replace("Cricket", "Football");
    	System.out.println(line);
    }

}
